#!/bin/bash

# Author: Fahad Alghathbar
# Created on: April 14, 2023

# اسم المجلد الذي سيتم إنشاء الملفات المصغرة فيه
output_folder="resized"
mkdir -p "$output_folder"

# الوقت الحالي قبل بداية تنفيذ البرنامج
start_time=$(date +%s)

# تكرار العمليات لكل ملف فيديو
for file in *.mp4
do
    echo "Resizing $file"
    ffmpeg -i "$file" -vf scale=iw/2:-1 "$output_folder/$file"
done

# الوقت بعد انتهاء تنفيذ البرنامج
end_time=$(date +%s)
duration=$((end_time - start_time))
echo "Finished resizing videos in $duration seconds."
